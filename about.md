+++
title = "about."
date = 2022-03-30
page_template = "page.html"

[extra]
active_key = "about"
+++

## me.

I am a seasoned Director of Engineering with a wealth of experience in leading cross-functional teams and delivering innovative solutions. Currently, I serve as Director of Engineering at Capsule8, where I oversee all Engineering operations and work closely with company heads and C-suite to define and execute company-wide strategic initiatives. I'm also a huge advocate of remote work and have established permanent remote-work collaboration standards. I currently reside in NYC, love dogs, and enjoy photography as a hobby.

Previously, I served as Director of Engineering at Narrativ, where I built the technology and team for Product Graph POC and drove the adoption of industry best practices. I am also a pro at migrations, and have led and advised migration plan from AWS ECS to Kubernetes.

My expertise lies in building and managing high-performing teams, driving innovation, and delivering results. I am an advocate of modern DevOps, SRE, and DevSecOps practices and am passionate about delivering customer-centric solutions that drive business growth. I am also a people-first leader, who is dedicated to helping my team members grow and succeed and is known for my fun-loving approach to work.

## this site.

Welcome to notmatthew.io, the virtual home of my wildest dreams and weirdest experiments. I created this site with the intention of sharing my projects, big and small, with the world and to hold myself accountable for actually finishing them. Because let's be real, we all have that one unfinished project collecting virtual dust in the corner of our hard drives.

But I don't want to be the only one sharing my work here. I am always on the lookout for like-minded individuals and their unique endeavors. So, whether you're working on something mind-blowing or just need a sounding board for your latest idea, feel free to reach out. Collaboration is the spice of life, after all.

Oh, and don't be shy to give me suggestions for this site. I'm open to constructive criticism and wild ideas. And if you just want to chat about something interesting, that works too.

Let's make things happen together!
