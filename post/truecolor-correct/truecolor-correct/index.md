+++
title = "truecolor support using mosh and tmux"
date = 2023-06-18
draft = false
[extra]
active_key = "blog"
+++

## assess your baseline.

There are a variety of technologies at play and each one needs to work end-to-end. The core of the debugging is an `awk` script [`termstandard/colors`](https://github.com/termstandard/colors) which will emit a color test (see example output below).

### script.

```awk
'BEGIN {
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
printf "\n";
}'
```

### example outputs.

#### correct output.

{{ post_image(img_name="truecolor-correct-20230618171944948.png",
              title="A correct, smooth gradient of colors"
			  alt="Screenshot of a bash terminal displaying a correct output of a color test script."
) }}

#### incorrect output.

{{ post_image(img_name="truecolor-incorrect-20230618171643302.png",
              title="An incorrect, blocky output.",
			  alt="Screenshot of a bash terminal displaying an incorrect output of a color test script."
) }}

Assert truecolor is working, using the above script, under:

1. The local shell, under the terminal emulator of choice. An even longer line that will wrap.
1. The remote shell, over SSH
1. The remote shell, over mosh
1. The remote shell, over mosh, inside of a `tmux` session
1. If needed: the local shell, inside of `tmux`

If you encounter an incorrect output, debugging

## final setup.

### software.

- [Kitty v0.28.1](https://github.com/kovidgoyal/kitty)
- [mosh v1.4](https://mosh.org)
	- For Ubuntu 22.04, mosh will need to be build from source. Download the `.tar.gz` and [follow the instructions under **Latest release**](https://mosh.org/#getting).
- [tmux v3.2a](https://tmux.github.io/)

### configurations.

#### .tmux.conf

The only change required for `tmux`.

```
set-option -ga terminal-overrides ",xterm-256color:Tc"
```

#### .bashrc

More of an "unconfiguration," do not set `$TERM` in your bash shell.
