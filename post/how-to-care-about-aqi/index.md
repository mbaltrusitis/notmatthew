+++
title = "how to care about aqi"
date = 2023-08-13
[taxonomies]
tags=["aqi"]
[extra]
active_key = "blog"
+++

I care about air quality now and you probably should too<!-- more -->.

## how i got here.

Masks have been a diminishing reality in my life as I learned to be comfortable with an endemic COVID-19 but the 2023 Canadian wildfires spurred a return of the familiar habit. A new ritual comes along with it: checking the day's [Air Quality Index](https://en.wikipedia.org/wiki/Air_quality_index) or, AQI.

## is it really that important?

Particle pollutions (PM2.5) has been linked to a variety of health issues. From the [CDC's website](https://www.cdc.gov/air/particulate_matter.html):

> Particle pollution has also been linked to:
> 
> - Eye irritation
> - Lung and throat irritation
> - Trouble breathing
> - Lung cancer
> - Problems with babies at birth (for example, low birth weight)

A [2019 study from Standford](https://www.ksfire.org/research/The%20impact%20of%20prescribed%20fire%20versus%20wildfire%20on%20the%20immune%20and%20cardiovascular%20systems%20of%20children.pdf) ([archive link](https://web.archive.org/web/20230728215704/https://www.ksfire.org/research/The%20impact%20of%20prescribed%20fire%20versus%20wildfire%20on%20the%20immune%20and%20cardiovascular%20systems%20of%20children.pdf)) really highlighted the seriousness of smoke exposure; linking a two-fold in the rate of asthma seen in children ages 0-5 when exposed to wildfire smoke for more than five days.

Other high-risk groups parallel to those we became familiar during the pandemic. This includes:

- adults over age of 65,
- pregnant people,
- people with a preexisting lung conditions (e.g., asthma, COPD)
- and as mentioned heretofore, children

Even if you are not in those higher-risk groups, Berkley Earth equated an AQI of "Unhealthy" to "Very Unhealthy" to smoking 15-20 cigarettes **a day**: something I already choose not to do.

## keeping a beat on aqi.

A variety of tools are available from both state and federal governments as well as a variety of companies with products on offer. There are two main ways I have come to think about air quality and that is the outdoor AQI and the indoor AQI.

### outdoor aqi

For anyone living in the United States the federal Environmental Protection Agency (EPA) has a dated but solid website (and app!) [AirNow.gov](https://www.airnow.gov/?city=Brooklyn&state=NY&country=USA). AirNow provides a straightforward visualization showing the AQI for today and the forecast for tomorrow. If you are trying to check one place and move-on from your life, download this app and move on with your life.

There are companies that provide a much slicker experience but to be honest seem to be not much more than novelty:

- [IQAir](https://www.iqair.com/us/usa) is a Swiss air quality company that sells various air filtering products and provides a more aesthetic experience than AirNow.gov but not something I more meaningfully helpful.
- [Zoom Earth](https://zoom.earth/) shows near real-time satellite images helpful for monitoring the path of incoming wildfire smoke

#### specific data for nyc

Living in NYC I make use of two additional data sources at NYC.gov's Environment and Health Portal:

- [Real-Time Air Quality of PM2.5 in NYC](https://a816-dohbesp.nyc.gov/IndicatorPublic/beta/key-topics/airquality/realtime/), provides hourly data across 8 locations of NYC in addition to the average of 8 additional DEC monitors
- [Neighborhood Air Quality Profiles](https://a816-dohbesp.nyc.gov/IndicatorPublic/beta/key-topics/airquality/aqe/), rates a neighborhoods air pollutant sources across the four categories of: building emissions, building density, industrial areas and traffic density. These profiles are not necessarily helpful day-to-day, they provide additional data points I certainly will factor into any future moves.

### indoor aqi

An additional insight gained from living through an air-borne, viral pandemic is the importance of air filtration and ventilation in indoor spaces. I have become fond of my home's HEPA filters through the additional motivation provided by Roux, our ~~precious~~ spoiled but shedding Australian Shepherd. I have been running a Winix AM90 24/7 for the last few years and can recommend them. The AM90s are able to integrate with [Home Assistant](https://www.home-assistant.io) through [iprak's winix component](https://github.com/iprak/winix).

How effective your HEPA filters are requires the additional step of deploying quality indoor air filters. The most basic of air quality monitors are focused on Volatile Organic Compounds (VOCs) which the type of indoor air pollution created when cooking with oil, especially frying foods, without sufficient air ventilation.

Monitoring whether outdoor air pollutants PM2.5/10s have made their way inside and settled there requires a more sophisticated device with costs ranging from $60-$100. A few companies that I found to have compelling product offerings are:

- [Air Gradient](https://www.airgradient.com/kits) ($67/$138), offers open source and hardware sensor kits in both preassembled and DIY varieties
- [Purple Air](https://www2.purpleair.com/products/purpleair-touch?variant=40366631551073) ($200), preassembled sensor that integrates nicely with a peer network that nicely maps regional air quality trends
- [Air Things](https://www.airthings.com/view-plus) ($299), preassembled sensors offering a similar level of PM monitoring with additional Radon (Rn) gas detection

**Note:** I have not received any sort of compensation for mentioning these companies. The above are based solely on my own, non-expert, research and what I would consider putting into my own home.

## wrapping up.

Everyones' risk profile (and their tolerance) are different. While events like this are likely to increase in the near future rather than decrease, I find it unbearable to listen to the news hype events like these and looked to what sorts of tools are available to me to enable sound, fact-based decisions based on potential risks. This note helped pull me process and pull together that information into a succinct way and hopefully can be helpful to others looking to do the same so we can move onto our lives.

Climate change is real.
