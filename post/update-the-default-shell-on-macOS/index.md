+++
title = "update the default shell on macOS"
date = 2023-06-10
[taxonomies]
tags=["shell","macos","how-to"]
[extra]
active_key = "blog"
+++

## overview.

Apple has shipped the Bourne Again Shell (i.e., bash) as the default shell for macOS for as long as I can remember but, starting with macOS Catalina (v10.15) Apple started shipping with Z Shell. Even before the switch to zsh an ancient version of bash would be shipped with macOS so it was not uncommon to change the default shell already.

A custom shell path can be set in terminals such as Kitty, Alacritty and the default Terminal.app but following the instructions below sets your default shell across the entire system for all terminal emulators.

## instructions.

### install a bash.

Using the brew package manager a more recent version of bash can be installed be executing `brew install bash`. After a successful installation find the path of the install by running the below command:

```sh
echo "$(brew --prefix)/bin/bash"
# /opt/homebrew/bin/bash
```

{% aside() %}
Upon the release of Apple's ARM processor the [homebrew project](https://github.com/Homebrew) updated their prefixes. You can review both the [discussion](https://github.com/Homebrew/brew/issues/9177) and [merge request](https://github.com/Homebrew/brew/pull/9117).
{% end %}

### update default user shell.

Add this value to the `/etc/shells` path so the above value may be passed to `chsh`. See the commands and their output as comments below:

```sh
MY_SHELL=$(echo "$(brew --prefix)/bin/bash" | sudo tee -a /etc/shells)
# Password for <username>:
chsh -s $MY_SHELL
# Changing shell for <username>.
# Password for <username>:
```

### restart your shell.

You can confirm your changes have taken by `echo`-ing the `SHELL` variable and confirming the value is the same as the previous value for `$MY_SHELL`. Personally, I had to fully logout of my session and then log back in to see the changes take.
