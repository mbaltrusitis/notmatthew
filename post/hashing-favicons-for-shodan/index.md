+++
title = "hashing favicons for shodan search"
date = 2024-01-06
[taxonomies]
tags=["elixir", "shodan", "scripting"]
[extra]
active_key = "blog"
+++

I recently needed to use [Shodan's](https://shodan.io) favicon search to get an idea what sort of device may be in a customer environment. I took this as an opportunity to play around scripting with Elixir over Python or Bash. A further explanation is provided under the code snippet below.

{{ source_link(url="https://gitlab.com/-/snippets/3636542") }}
{{ gitlab_snippet(url="https://gitlab.com/-/snippets/3636542", file_type="elixir", filename="favicon_hash.exs")  }}

## going deeper.

Elixir's [`Base`](https://hexdocs.pm/elixir/1.15/Base.html) module does not have a MIME base64 implementation like Python's [`base64.encodebytes`](https://docs.python.org/3/library/base64.html#base64.encodebytes) builtin but it's an easy enough [spec](https://www.ietf.org/rfc/rfc2045.txt) to implement. The base64 encoding needs to have a newline inserted every 76 characters and at the very end.

Lastly, after getting the the [MurmurHash](https://en.wikipedia.org/wiki/MurmurHash) it must be cast as a 32-bit signed integer in order to be properly accepted by Shodan's search API.

## wrapping up.

{% aside() %}
Backwards compatibility is great anyways with Elixir and its unlikely my scripts will break across a few minor versions.
{% end %}

While Elixir is not an obvious choice for scripting, the change of pace and avoiding the dependency headaches that come with Python is nice. Its much easier to keep a global [`asdf`](https://asdf-vm.com) config in my `$HOME`, constraining scripts to a version of Elixir versus the bag of snakes that comes with installing dependencies in a System Python. I will leave that particular headache for the package maintainers.

Lets see where else I can apply this stack this year.

Until next time.

!matthew
