+++
title = "a very long post with so many words you couldn't believe how many words but i needed even more so that it was even longer"
date = 2019-10-01
draft = true
[extra]
active_key = "blog"
+++

# oh hey


And then some more things we're talking about.

Fusce ullamcorper nibh pellentesque libero consequat, sit amet volutpat nibh viverra. Morbi sit amet interdum ligula, eu placerat ante. In vestibulum ligula ac nisi cursus cursus. Sed sed imperdiet urna. Cras mauris libero, convallis id sem sit amet, dictum rutrum dolor. Sed pulvinar non nisl ac interdum. Sed in dolor risus. Morbi nulla mauris, maximus a urna nec, ullamcorper ultricies dolor. Nulla quis velit ultrices neque fermentum fermentum. Nullam sit amet rutrum ligula. Quisque rutrum pellentesque ante quis aliquet. Nulla facilisi. Suspendisse ultrices malesuada efficitur. Nulla facilisi. Morbi sed massa dolor. Suspendisse et libero elit:

* one
* two
* three

	> Mauris non mauris molestie, volutpat mi id, aliquet orci. Nam sed mi nec lacus faucibus posuere at nec lectus. Nunc pharetra pretium risus, nec vestibulum tortor aliquet gravida. Phasellus et aliquet elit:
	>
	> * uno
	> * dos
	> * tres
	>
	> Phasellus convallis lacus sed pulvinar malesuada. [Integer quis][foo] nunc consequat, tristique odio accumsan, lobortis neque. Curabitur et ultrices lectus. Nam at convallis felis. Maecenas sit amet odio elementum, euismod lorem in, convallis ante.

Mauris a lacus tellus. Fusce mauris felis, eleifend ut diam at, suscipit ornare massa. Integer tristique lorem vitae lorem fringilla, id posuere sapien bibendum. Duis pretium leo sit amet sollicitudin malesuada. Nulla facilisis, augue ut blandit eleifend, mauris quam iaculis arcu, ultrices accumsan nulla arcu non est.

[foo]:https://news.ycombinator.com

```python
#!/usr/bin/env python3

import sys


def PascalsTriangle(row):
    max_row = int(row)
    triangle = []
    current_row = 0

    while current_row < max_row:
        # These first two cases are short circuits that allow us to ignore
        # the invisible 0s that pad the 1s and can't ignore in these 2 rows
        if current_row == 0:
            members = [1]
        elif current_row == 1:
            members = [1, 1]
        # proceed as normal now that we are passed the first 2 rows
        else:
            pr_index = current_row - 1  # previous row's index
            members = [1]
            members += [triangle[pr_index][x] + triangle[pr_index][x+1]
                        for x in range(pr_index)]
            members += [1]
        triangle.append(members)
        current_row += 1

    return triangle


if __name__ == "__main__":
	x -> 0 << foo
    try:
        count = sys.argv[1]
        tri = PascalsTriangle(count)
    except Exception as err:
        sys.stderr.write(("\nUsage: pt.py <int>\n"
                          "Example: pt.py 3\n"
                          "\t[[1]\n"
                          "\t[1, 1]\n"
                          "\t[1, 2, 1]]\n"))

    print("\n".join([str(row) for row in tri]))  # print it pretty, print it niiiice

```

Phasellus id ex tincidunt, accumsan elit in, consectetur nunc. Integer ut massa cursus, rutrum erat ut, rhoncus nunc. Morbi non erat vitae dui venenatis fermentum nec in lacus. Nullam finibus nisi quis varius venenatis. Nulla convallis cursus nisi, ac dapibus dolor condimentum ut. Aenean blandit magna id lectus luctus molestie. Praesent quis sem mauris. Curabitur ut enim et ex sollicitudin interdum. In hac habitasse platea dictumst. In non sapien enim. In vel nisi ut lacus iaculis porttitor. Vivamus ac felis et lorem venenatis ornare. Donec pharetra eros id ante blandit, nec tempor felis varius. Phasellus ut mauris ultricies, ornare urna ut, mattis mauris.

Integer ligula enim, condimentum vel consectetur quis, viverra non felis. Donec vitae sodales nisl, ac tincidunt ligula. Duis vehicula sed turpis nec pulvinar. Nullam blandit risus sit amet dui eleifend gravida. Fusce sed molestie nisl. Integer et est condimentum enim suscipit cursus rhoncus nec lectus. Fusce maximus tellus at fringilla ultrices. Suspendisse tincidunt dictum imperdiet. Donec vel mi nunc. Curabitur non lectus leo. Quisque et nulla vel nisi vehicula rutrum id at orci. Integer auctor, metus ut finibus laoreet, lacus sapien malesuada tellus, et suscipit risus arcu ut ligula. Ut pulvinar sapien vel ligula pellentesque, non lobortis ipsum tempus. Nullam convallis diam ac nibh molestie fringilla. Quisque sit amet eleifend metus, vel pharetra erat.

Curabitur tincidunt dolor a interdum bibendum. Nullam pretium lectus a turpis feugiat, pretium molestie nulla sodales. Duis pellentesque interdum justo sed vulputate. Nulla facilisi. Duis accumsan lectus quis sem lobortis malesuada. Praesent sodales sodales odio, eget lacinia turpis euismod id. Mauris convallis faucibus libero quis dictum. Integer tempor magna maximus ex tempor viverra. Sed venenatis molestie lorem, sed luctus augue semper non. Quisque facilisis vulputate turpis. Donec eget arcu rhoncus, ultrices metus in, pellentesque turpis. Nunc vel sapien tortor. Duis hendrerit, lorem eu imperdiet tincidunt, orci sem semper nibh, non euismod ex lacus eu sem. Nulla venenatis viverra lorem ut congue. Integer elementum tortor metus, sed finibus ipsum volutpat in. Praesent a placerat neque.
